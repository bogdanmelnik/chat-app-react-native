import React, {Component} from "react";
import PropTypes from 'prop-types';

import { GiftedChat } from 'react-native-gifted-chat';

export default class Chat extends Component{
    state = {
        messages: [],
    };

    componentWillMount() {
        const name = 'Hello ' + this.props.nameText;
        this.setState({
            messages: [
                {
                    _id: 1,
                    text: name,
                    createdAt: new Date(),
                    user: {
                        _id: 2,
                        name: 'React Native',
                    },
                },
            ],
        })
    }
    onSend(messages = []) {
        this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, messages),
        }))
    }
    render(){
        return (
            <GiftedChat
                messages={this.state.messages}
                onSend={messages => this.onSend(messages)}
                user={{
                    _id: 1,
                }}
            />
        );
    }

}
Chat.defaultProps = {
    nameText: 'John',
};
Chat.propTypes = {
    nameText: PropTypes.string,
};