import React, {Component} from "react";

import {
    View,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity
} from 'react-native';

import {Actions} from 'react-native-router-flux';

const styles = StyleSheet.create({
    title: {
        marginTop: 20,
        marginLeft: 20,
        fontSize: 20,
    },
    nameInput: {
        height: 40,
        borderWidth: 2,
        borderColor: 'grey',
        margin: 20,
        padding: 5,
    },
    buttonText: {
        marginLeft: 20,
        fontSize: 20,
    }
});

export default class Home extends Component{
    state = {
        name: null
    };
    render(){
        return (
            <View>
                <Text style={styles.title}>Enter your name :</Text>
                <TextInput
                placeholder="Bogdan Melnik"
                style={styles.nameInput}
                value={this.state.name}
                onChangeText={(text) => {
                    this.setState({
                        name: text
                    });
                }}/>
                <TouchableOpacity onPress={() => {
                    Actions.chat({
                        nameText: this.state.name
                    })
                }}>
                    <Text style={styles.buttonText}>Next</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
